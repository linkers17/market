
const gulp = require('gulp');
var rename = require('gulp-rename');

var cssmin = require('gulp-cssmin');

var jsmin = require('gulp-jsmin');

var browserSync = require('browser-sync').create();

var concat = require('gulp-concat');


var less = require('gulp-less');
var LessAutoprefix = require('less-plugin-autoprefix');
var autoprefix = new LessAutoprefix({ browsers: ['last 2 versions'] });
var sourcemaps = require('gulp-sourcemaps');

'use strict';
var sass = require('gulp-sass');
sass.compiler = require('node-sass');

var smartgrid = require('smart-grid');

/*
gulp.task('task-css', function() {
	gulp.src('./css/*.css')
		.pipe(cssmin())
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest('./build/css'));

});
*/
gulp.task('task-less', function(){
	return gulp.src('./less/**/*.less')
		.pipe(sourcemaps.init())
		.pipe(less({plugins: [autoprefix]}))
		.pipe(sourcemaps.write('./maps'))
		.pipe(cssmin())
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest('./build/css'));
});

gulp.task('task-sass', function(){
	return gulp.src('./sass/**/*.sass')
	.pipe(sass().on('error', sass.logError))
	.pipe(cssmin())
	.pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('./build/css'));
});

 
gulp.task('task-js', function () {
    gulp.src('./js/*.js')
        .pipe(jsmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./build/js'));
});

gulp.task('build-fontawesome-css', function(){
	return gulp.src('./node_modules/font-awesome5/css/fontawesome-all.min.css')
	.pipe(gulp.dest('./build/css'));
});

gulp.task('build-fontawesome-ico', function(){
	return gulp.src('./node_modules/font-awesome5/webfonts/**')
	.pipe(gulp.dest('./build/webfonts'));
});

gulp.task('build', ['task-js', 'build-fontawesome-css', 'build-fontawesome-ico']);


gulp.task('watch-files', ['browser-sync'], function(){
	gulp.watch('./**/*.html').on('change', browserSync.reload);
	gulp.watch('./**/*.less', ['task-less']).on('change', browserSync.reload);
	gulp.watch('./**/*.sass', ['task-sass']).on('change', browserSync.reload);
});

gulp.task('concat', function() {
	return gulp.src(['./less/var.less', './less/template.less', './less/product_style.less'])
	  .pipe(concat('all.less'))
	  .pipe(gulp.dest('./less/'));
});

// Static server
gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./build"
        },
		browser: 'chrome'
    });
});



// Smart-grid

gulp.task('generate-grid', function () {

	var settings = {
	    outputStyle: 'sass', /* less || scss || sass || styl */
	    columns: 12, /* number of grid columns */
	    offset: '30px', /* gutter width px || % || rem */
	    mobileFirst: false, /* mobileFirst ? 'min-width' : 'max-width' */
	    container: {
	        maxWidth: '1200px', /* max-width оn very large screen */
	        fields: '15px' /* side fields */
	    },
	    breakPoints: {
	        lg: {
	            width: '1100px', /* -> @media (max-width: 1100px) */
	        },
	        md: {
	            width: '960px'
	        },
	        sm: {
	            width: '780px',
	            fields: '15px' /* set fields only if you want to change container.fields */
	        },
	        xs: {
	            width: '560px'
	        }
	        /* 
	        We can create any quantity of break points.
	 
	        some_name: {
	            width: 'Npx',
	            fields: 'N(px|%|rem)',
	            offset: 'N(px|%|rem)'
	        }
	        */
	    }
	};
 
	smartgrid('./sass', settings);
});

