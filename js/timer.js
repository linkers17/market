window.onload = function() {
   initializeTimer();
}

function initializeTimer() {

   var endDate = new Date(2019, 4, 13);
   var currentDate = new Date();

   var seconds = (endDate - currentDate) / 1000;

   if (seconds > 0) {

      var minutes = seconds / 60;
      var hours = minutes / 60;
      
      hours = Math.floor(hours);
      minutes = Math.floor(minutes) - hours * 60;
      seconds = Math.floor(seconds) - (hours * 3600 + minutes * 60);

      setTimeDate(hours, minutes, seconds);

      function secOut() {

         if (seconds == 0) {

            if (minutes == 0) {

               if (hours == 0) {



               } else {

                  hours--;
                  minutes = 59;
                  seconds = 59;

               }

            } else {

               minutes--;
               seconds = 59;

            }

         } else {

            seconds--;

         }

         setTimeDate(hours, minutes, seconds);

      }

      setInterval(secOut, 1000);

   } else {

      console.log('время истекло');

   }

   console.log(minutes, hours);
   
}

function setTimeDate(hr, min, sec) {
   var hour = document.querySelector('.hour span');
   var minute = document.querySelector('.minute span');
   var second = document.querySelector('.second span');

   hour.innerHTML = hr;
   minute.innerHTML = min;
   second.innerHTML = sec;
}