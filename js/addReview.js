var rating = 3;         // количество звезд

$('#one').on('click', function(){

   rating = 1;

})

$('#two').on('click', function(){

   rating = 2;

})

$('#three').on('click', function(){

   rating = 3;

})

$('#four').on('click', function(){

   rating = 4;

})

$('#five').on('click', function(){

   rating = 5;

})

var name;
var email;
var text;

var flag = false;

$('.btn-form').prop('disabled', true);

$('#name').focusout(function(){

   name = $('#name').val();

   if (name != '') {

      flag = true;
      $('.btn-form').prop('disabled', false);

   } else {

      flag = false;
      $('.btn-form').prop('disabled', true);

   }

});

$('#email').focusout(function(){

   email = $('#email').val();

   if (email != '') {

      flag = true;
      $('.btn-form').prop('disabled', false);

   } else {

      flag = false;
      $('.btn-form').prop('disabled', true);

   }

});

$('#review').focusout(function(){

   text = $('#review').val();

   if (text != '') {

      flag = true;
      $('.btn-form').prop('disabled', false);

   } else {

      flag = false;
      $('.btn-form').prop('disabled', true);

   }

});

$('.btn-form').on('click', function(){

   // проверка полей на заполнение
   $('.enter').each(function(){
      
      if ($(this).val() == '') {

         $(this).addClass('error');

      } else {

         $(this).removeClass('error');

      }

   });

   if ((name != '') && (email != '') && (text != '') && flag) {

      $.post('obr.php',
      {
         name: name,
         email: email,
         text: text,
         rating: rating
      },
         onAjaxSuccess
      );

   }

});

function onAjaxSuccess(data) {
   $('.comments-wrap').append(data);
   $('#name').val('');
   $('#email').val('');
   $('#review').val('');
   name = '';
   email = '';
   text = '';
   $('#star-3').attr('checked', 'checked');
   rating = 3;
   
}